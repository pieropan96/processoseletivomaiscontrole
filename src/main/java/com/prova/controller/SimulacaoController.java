package com.prova.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.prova.dominio.Simulacao;
import com.prova.repository.SimulacaoRepository;

@Controller
@RequestMapping("/simulacao")
public class SimulacaoController {

	@Autowired
	private SimulacaoRepository simulacaoRepository;

	@RequestMapping(method = RequestMethod.GET, value = ("/"))
	public ModelAndView simulacoes() {
		ModelAndView view = new ModelAndView("simulacao/simulacao");
		Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
		view.addObject("simulacoes", simulacoes);
		return view;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ModelAndView verResultado(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("simulacao/simulacao_resultado");
		Optional<Simulacao> simulacao = simulacaoRepository.findById(id);
		view.addObject("simulacao", simulacao.get());
		view.addObject("imovel", simulacao.get().getImovel());
		view.addObject("comprador", simulacao.get().getComprador());
		return view;
	}

}
