package com.prova.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.prova.dominio.Imovel;
import com.prova.repository.ImovelRepository;

@Controller
public class IndexController {

	@Autowired
	private ImovelRepository imovelRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/index");
		view.addObject("imoveis", buscarImoveis());
		return view;
	}

	private Iterable<Imovel> buscarImoveis() {
		Iterable<Imovel> imoveis = imovelRepository.findAll();
		if (!imoveis.iterator().hasNext()) {
			imoveis = popularBancoDeDados();
		}
		return imoveis;
	}

	private Iterable<Imovel> popularBancoDeDados() {
		List<Imovel> imoveis = new ArrayList<>();
		Imovel imovel = new Imovel("Reserva Bandeirantes", "Reserva Bandeirantes - Minha Casa Minha Vida",
				"Bandeirante - Juiz de Fora", new BigDecimal("190.0000"), "https://www.encurtador.com.br/dot58");
		Imovel imovel2 = new Imovel("Park Quinet", "Park Quinet - Minha Casa Minha Vida",
				"Santa Terezinha - Juiz de Fora", new BigDecimal("210.0000"), "https://www.encurtador.com.br/bikPY");
		imoveis.add(imovel);
		imoveis.add(imovel2);
		imovelRepository.saveAll(imoveis);
		return imovelRepository.findAll();
	}
}
