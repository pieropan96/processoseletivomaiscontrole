package com.prova.controller;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.prova.dominio.Comprador;
import com.prova.dominio.Imovel;
import com.prova.dominio.Simulacao;
import com.prova.repository.CompradorRepository;
import com.prova.repository.ImovelRepository;
import com.prova.repository.SimulacaoRepository;

@Controller
@RequestMapping("/imoveis")
public class ImovelController {

	@Autowired
	private ImovelRepository imovelRepository;

	@Autowired
	private CompradorRepository compradorRepository;

	@Autowired
	private SimulacaoRepository simulacaoRepository;

	@RequestMapping(value = "/{id}")
	public ModelAndView detalhe(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("imovel/imovel");
		Optional<Imovel> imovel = imovelRepository.findById(id);
		view.addObject("imovel", imovel.get());
		return view;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	public ModelAndView salvar(@PathVariable("id") Integer id, Comprador comprador) {
		ModelAndView view = new ModelAndView("redirect:/");
		salvarSimulacao(comprador, id);
		view.addObject("imovel", imovelRepository.findAll());
		return view;
	}

	private void salvarSimulacao(Comprador comprador, Integer id) {
		Comprador c = salvarComprador(comprador);
		Optional<Imovel> imovel = buscarImovel(id);
		Object[] d = dados();
		Simulacao simulacao = new Simulacao(imovel.get(), c, (boolean) d[1], (String) d[0]);
		simulacaoRepository.save(simulacao);
	}

	public Object[] dados() {
		Object[] dados = new Object[2];
		dados[1] = false;
		String mensagem = "";
		Random gerador = new Random();
		int valor = gerador.nextInt(3);
		if (valor == 0) {
			mensagem = "Nome negativado.";
		} else if (valor == 1) {
			mensagem = "Baixa renda.";
		} else {
			mensagem = "Aprovado.";
			dados[1] = true;
		}
		dados[0] = mensagem;
		return dados;

	}

	private Comprador salvarComprador(Comprador comprador) {
		return compradorRepository.save(comprador);
	}

	private Optional<Imovel> buscarImovel(Integer id) {
		return imovelRepository.findById(id);
	}
}