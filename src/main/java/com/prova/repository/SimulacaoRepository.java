package com.prova.repository;

import org.springframework.data.repository.CrudRepository;

import com.prova.dominio.Simulacao;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

}