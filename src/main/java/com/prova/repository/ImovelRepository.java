package com.prova.repository;

import org.springframework.data.repository.CrudRepository;

import com.prova.dominio.Imovel;

public interface ImovelRepository extends CrudRepository<Imovel, Integer> {

}