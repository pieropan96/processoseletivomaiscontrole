package com.prova.repository;

import org.springframework.data.repository.CrudRepository;

import com.prova.dominio.Comprador;

public interface CompradorRepository extends CrudRepository<Comprador, Integer> {

}